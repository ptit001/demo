<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
class isSupOrAd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Auth::check() or ($request->user()->role!=Config::get('constant.SUPERADMIN') and $request->user()->role!=Config::get('constant.ADMIN'))){
            return redirect('/');
        }  
        return $next($request);
    }
}
