<?php

namespace App\Http\Middleware;

use Closure;

use App\Demo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class isRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //var_dump(Demo::findById($request->route()->getParameter('id'))->role);die;
        //var_dump($request->route()->getParameter('id'));die;
        //var_dump(Config::get('constant.ADMIN'));die;
         if($request->user()->role==Config::get('constant.ADMIN') and 
                 (Demo::findById($request->route()->getParameter('id'))->role==Config::get('constant.SUPERADMIN')
                        or Demo::findById($request->route()->getParameter('id'))->role==Config::get('constant.ADMIN'))){
            return redirect('/');
        }  
        return $next($request);
    }
}
