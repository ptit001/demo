<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Demo;
use App\Profile;
use App\Http\Requests\ProfileRequest;
use Illuminate\Support\Facades\Config;
class ProfileController extends Controller
{
    public function profile($id) {
        $demo=Demo::findById($id);
        $profile=$demo->profile;
        if($profile==null){
            return redirect()->route('createprofile',['id'=>$id]);
        }
        else{
        return view('demo.profile',['profile'=>$profile,'demo'=>$demo]);}
    }
    public function createprofile($id){
        $demo=Demo::findById($id);
        return view('demo.createprofile',['demo'=>$demo]);
    }
    public function editprofile($id) {
        $demo=Demo::findById($id);
        $profile=$demo->profile;
        return view('demo.editprofile',['profile'=>$profile,'demo'=>$demo]);
    }
    public function posteditprofile(ProfileRequest $request,$id){
         $profile=Demo::findById($id)->profile;
        if($request->hasFile('file')){
            $ava=$request->file('file');
            $name=$ava->getClientOriginalName();
            $ava->move('uploads',$name);}
        else $name= $profile->avatar; 
            $input=[
                'user_id'=>$id,
                'dob'=>$request['dob'],
                'address'=>$request['address'],
                'phone'=>$request['phone'],
                'avatar'=>$name,
            ];
            
        Profile::updateById($input,$profile->id);
            
        
        return redirect()->route('profile',['id'=>$id]);
    }
    public function postcreateprofile(ProfileRequest $request,$id) {
        if($request->hasFile('file')){
            $ava=$request->file('file');
            $name=$ava->getClientOriginalName();
            $ava->move('uploads',$name);}
                        
        else $name=  Config::get('constant.avatar');

            $input=[
                'user_id'=>$id,
                'dob'=>$request['dob'],
                'address'=>$request['address'],
                'phone'=>$request['phone'],
                'avatar'=>$name,
            ];
      
            Profile::insert($input);
        
        return redirect()->route('profile',['id'=>$id]);
    }
}
