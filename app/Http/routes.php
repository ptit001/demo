<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Database\Eloquent\ModelNotFoundException;
Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'ArticleController@articles');
    
    Route::auth();
     Route::get('demo/{user_id}/detail/{article_id}',[
        'as'=>'detail','uses'=>'ArticleController@detail',
    ]);
    Route::group(['middleware'=>'issuporad'],function(){
        Route::get('/demo/create',[
        'as'=>'getcreate', 'uses'=>'DemoController@create'
    ]);
        Route::post('/demo/create',[
        'as'=>'postcreate', 'uses'=>'DemoController@store'
    ]);
    
    Route::get('/demo/{id}/edit',[
        'middleware'=>'isrole',
        'as'=>'edit','uses'=>'DemoController@edit'
    ]);
    Route::post('/demo/{id}/update',[
        'middleware'=>'isrole',
        'as'=>'update','uses'=>'DemoController@update'
    ]);
    Route::get('demo/{id}/delete',[
        'middleware'=>'isrole',
        'as'=>'delete','uses'=>'DemoController@delete'
    ]);
    Route::get('demo/{id}/post',[
        'as'=>'postarticle','uses'=>'ArticleController@viewpost',
    ]);
    Route::post('demo/{id}/post',[
        'as'=>'post','uses'=>'ArticleController@post',
    ]);
     Route::get('demo/{id}/list',[
        'as'=>'listarticle','uses'=>'ArticleController@listpost',]);
    Route:: get('demo/edit/{id}/article',[
         'middleware'=>'isauthor',
        'as'=>'editarticle','uses'=>'ArticleController@edit',
    ]);
     Route:: post('demo/edit/{id}/article',[
        
        'as'=>'posteditarticle','uses'=>'ArticleController@postedit',
    ]);
     Route:: get('demo/delete/{id}/article',[
         'middleware'=>'isauthor',
        'as'=>'deletearticle','uses'=>'ArticleController@delete',
    ]);
    });
    Route::group(['middleware'=>'isuser'],function(){
        Route::get('/demo',[
        
        'as'=>'list', 'uses'=>'DemoController@index'
    ]);
   Route::get('demo/{id}/profile',[
       
       'as'=>'profile','uses'=>'ProfileController@profile',
   ]);
   
    Route::get('demo/{id}/editprofile',[
        'middleware'=>'isowner',
        'as'=>'editprofile','uses'=>'ProfileController@editprofile',
    ]);
    Route::post('demo/{id}/editprofile',[
        'middleware'=>'isowner',
        'as'=>'posteditprofile','uses'=>'ProfileController@posteditprofile',
    ]);
    Route::get('demo/{id}/createprofile',[
        'middleware'=>'isowner',
        'as'=>'createprofile','uses'=>'ProfileController@createprofile',
    ]);
    Route::post('demo/{id}/createprofile',[
        'middleware'=>'isowner',
        'as'=>'postcreateprofile','uses'=>'ProfileController@postcreateprofile',
    ]);
   
   });
    
    });
    
   
    
   




Route::get('/home', 'HomeController@index');
