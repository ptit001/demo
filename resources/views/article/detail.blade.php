@extends('layouts.app')
@section('content')
<div class='container'>
    <div class=" col-md-offset-2">
<div class="row">
                
                <div class="col-md-9">
                    <img style="width: 100%;height: 300px" class="img-responsive" src="{{asset('uploads/')}}/{{$article->image}}">
                    <h3>{{$article->title}} <small>Posted By {{$demo->name}}</small></h3>
                    <p class="glyphicon glyphicon-time">  {{$article->updated_at}}</p>
                    <p><i>{{$article->description}}</i></p><hr>
                    <p>{{$article->detail}}</p>
                    
                </div>
            </div>
</div>
</div>
<hr>

@endsection

