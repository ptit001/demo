@extends('layouts.app')
@section('content')

<div class='col-md-8 col-md-offset-2'>
    <div class="panel panel-default">
      <div class="panel-heading">Create {{$demo->name}}'s Profile</div>
      <div class="panel-body">
            <form class='form-horizontal' action="{{route('postcreateprofile',['id'=>$demo->id])}}" method="Post" enctype="multipart/form-data">
                <input type='hidden' name="_token" value='{{csrf_token()}}'>
                <div class='form-group {{ $errors->has('dob') ? ' has-error' : '' }}'>
                    <label class="col-md-4 control-label">Birthday</label>
                    <div class='col-md-6'>
                    <input type='date' class='form-control'  name='dob' value='{{old('dob')}}'>
                    @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob')}}</strong>
                                    </span>
                                @endif
                    </div>
                </div>
                <div class='form-group {{ $errors->has('address') ? ' has-error' : '' }}'>
                    <label class="col-md-4 control-label">Address</label>
                    <div class='col-md-6'>
                    <input type='text' class='form-control'  name='address' value='{{old('address')}}'>
                    @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address')}}</strong>
                                    </span>
                                @endif
                    </div>
                </div>
                <div class='form-group {{ $errors->has('phone') ? ' has-error' : '' }}'>
                    <label class="col-md-4 control-label">Phone</label>
                     <div class='col-md-6'>
                    <input type='text' class='form-control'  name='phone' value='{{old('phone')}}'>
                      @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone')}}</strong>
                                    </span>
                                @endif
                </div>
                </div>
                <div class="form-group {{ $errors->has('file') ? ' has-error' : '' }}">
                    
                    <label class="col-md-4 control-label">Upload Avatar</label>
                     <div class='col-md-6'>
                    <input class='form-control' id="uploadFile" placeholder="Choose File" disabled="disabled" />
                    <label  class="custom-file-input control-label" >
                         <input type="file" id="uploadBtn" name="file" style="padding-top:5px">
                    </label>
                    @if ($errors->has('file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file')}}</strong>
                                    </span>
                                @endif
                     </div>
                </div>    
                <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class='btn btn-primary'>Create</button>
                            </div>
            </form>
        </div>
    </div>
</div>
</div>
 <script src="{{ asset('js/style.js') }}"></script>
@endsection